package controllers

import (
    "github.com/dalstonalex/webapp-0817/views"
	"net/http"
	"fmt"
)

func NewGalleries() *Galleries {
	return &Galleries{
		NewView: views.NewView("bootstrap", "galleries/new"),
	}
}

type Galleries struct {
	NewView *views.View
}

// GET /creategallery
func (u *Galleries) New(w http.ResponseWriter, r *http.Request) {
	if err := u.NewView.Render(w, nil); err != nil {
		panic(err)
	}
}

type CreategalleryForm struct {
	Album    string `schema:"email"`
	Password string `schema:"password"`
}

// Create is used to process the signup form when a user
// tries to create a new user account.
//
// POST /signup
func (u *Galleries) Create(w http.ResponseWriter, r *http.Request) {
	var form CreategalleryForm
	if err := parseForm(r, &form); err != nil {
		panic(err)
	}
	fmt.Fprintln(w, "Album is", form.Album)
	fmt.Fprintln(w, "Password is", form.Password)
}

